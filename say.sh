function say {
	if test -f $1; then
		CMD="cat"  # Read file
	else
		CMD="echo" # Read using input
	fi

	$CMD $1 | nanotts-git --play --pitch 0.75 --volume 0.65 --speed 0.95
}
