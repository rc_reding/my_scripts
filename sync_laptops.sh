function synchronise {
	case "${1:u}" in
		"MAC")
			HOST="carlos@192.168.0.11:"
			RMT_DEST="~/Documents/college"
			DEST="${HOST}${RMT_DEST}"
			SRC="/home/carlos/Documents/college"
			if [ -d "$DEST" ]; then
				PROFILE="mac"
			fi
			;;
		"ZEPHYRUS")
			HOST="carlos@192.168.0.11:"
			RMT_SRC="~/Documents/college"
			SRC="${HOST}${RMT_SRC}"
			DEST="/home/carlos/Documents"
			if [ -d "$DEST" ]; then
				PROFILE="asus"
			fi
			;;
		*)
			echo -e "The profile doesn't exist. Available profiles are: \e[3mhome\e[0m, \e[3m work \e[0m, and \e[3mclone\e[0m (home)."
			return 1
			;;
	esac
	
	if [ -n "${PROFILE}" ]; then
		# RSYNC options
		RSYNC_ARGS="--recursive --update --links --perms --executability --times --group --owner --human-readable --info=progress2 --exclude 'software/'"
		
		# Do backup.
		printf "Initialising \e[1m\`${PROFILE}'\e[0m backup...\n\r"
		
		eval /usr/bin/rsync $RSYNC_ARGS $SRC $DEST --delete-after
		
		printf "\nBackup complete.\n"
		unset $PROFILE
		return 0
	else
		printf "Unable to perform backup: Destination not mounted.\n"
	fi

	unset $PROFILE
}

